# bashWrapper

A simple bash command wrapper that includes a stacktrace on error.

Provides the following:

1. Echos all commands ran via the 'run' wrapper
2. Provides a stacktrace on any error ran through the 'run' wrapper

Verified working on: **GNU bash, version 5.0.17(1)-release (x86_64-pc-linux-gnu)**
