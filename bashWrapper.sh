#!/bin/bash

set -o pipefail   # Prevents pipes from eating exit codes
shopt -s extdebug # Needed for use of ${BASH_ARGV}

script_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
. ${script_dir}/colors.sh true

dump_stack() {
  local i=0
  local line_no
  local function_name
  local file_name
  local k=0
  while caller $k >/dev/null 2>&1; do
    ((k++))
  done
  main_arg=$(echo "${BASH_ARGV[@]:$k-1}" | awk '{ for (i=NF; i>1; i--) printf("%s ",$i); print $1; }') # reversing the order of the args so they match how they actually were used
  echo -e "${RED}STACKTRACE:${ENDC}"
  # Rewriting this while loop non-sense to make sense will only break it, yes, even the weirdness with the double i++
  while caller $i; do
    ((i++))
  done | while read -r line_no function_name file_name; do
    if [[ $i -eq $k-1 ]]; then
      echo -e "\t${CYAN}$file_name:$line_no\t${GREEN}$function_name${PURPLE} \"${main_arg[*]}\"${ENDC}"
      break
    fi
    echo -e "\t${CYAN}$file_name:$line_no\t${GREEN}$function_name${PURPLE} \"${BASH_ARGV[$i]}\"${ENDC}"
    ((i++))
  done
}

function run() {
  if [ $# -ne 1 ]; then
    echo -e "${RED}[FATAL]: 'run' TAKES ONE ARG${ENDC}"
    exit 1
  fi
  cmd=$1
  echo -e "[${CYAN}$0${ENDC}]: ${cmd}"
  eval "${cmd}"
  result=$?
  if [[ ${result} -ne 0 ]]; then
    call_info=($(caller))
    echo -e "${RED}<[${CYAN}${call_info[1]}:${call_info[0]}${RED}](Errno=${result})> ${BLUE}'${cmd}'${RED} FAILED!${ENDC}" >&2
    dump_stack
    exit ${result}
  fi
}
