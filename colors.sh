#!/bin/bash

if [[ $1 == true ]]; then
  ENDC="\e[0m"
  NC="\e[0m"
  BLACK="\e[90m"
  GRAY="\e[37m"
  RED="\e[91m"
  GREEN="\e[92m"
  YELLOW="\e[93m"
  BLUE="\e[94m"
  PURPLE="\e[95m"
  CYAN="\e[96m"
  WHITE="\e[97m"
elif [[ $1 == false ]]; then
  ENDC=""
  NC=""
  BLACK=""
  GRAY=""
  RED=""
  GREEN=""
  YELLOW=""
  BLUE=""
  PURPLE=""
  CYAN=""
  WHITE=""
else
  echo -e "[FATAL]: Invalid or missing arguement!"
  exit 1
fi
