#!/bin/bash

. bashWrapper.sh

function call() {
  run "$1"
}

test_file="test_bashWrapper.log"
call "printenv" >/dev/null 2>&1
call "touch ${test_file}"
call "echo \"Hello, World!\" > ${test_file}"
call "echo \"Goodbye, World!\" | tee -a ${test_file}"
call "printenv | wc -l"
call "cat ${test_file}"
call "rm ${test_file}"
call "cat ${test_file}"
